import { useSelector } from "react-redux";
import { RootState } from "../store";
import Login from "../features/login/Login";
import Counter from "../features/counter/Counter";
import Header from "../components/Header";

export default function Main() {
  const loginstate = useSelector((state: RootState) => state.login.value);

  return (
    <div>
      <Header />
      <Login />
      {loginstate ? <Counter /> : null}
    </div>
  );
}
