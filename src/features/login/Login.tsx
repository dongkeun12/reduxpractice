import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "../../store";
import {login, logout} from './loginSlice'

const Login = () => {
  const loginstate = useSelector((state: RootState) => state.login.value);
  const dispatch = useDispatch<AppDispatch>();

  if(!loginstate){
    return <button onClick={() => dispatch(login())}>로그인</button>
  }else{
    return <button onClick={() => dispatch(logout())}>로그아웃</button>
  }
};

export default Login;