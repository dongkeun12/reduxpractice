import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "../../store";
import {
  decrement,
  increment,
  incrementAsync,
  incrementByAmount,
} from "./counterSlice";

const Counter = () => {

  const count = useSelector((state: RootState) => state.counter.value);
  const dispatch = useDispatch<AppDispatch>();

  return (
    <>
      <div>Redux Tutorial</div>
      <br />
      <div>{count}</div>
      <div>
        <button onClick={() => dispatch(increment())}>+</button>
        <button onClick={() => dispatch(incrementByAmount(10))}>+10</button>
        <button onClick={() => dispatch(incrementAsync(100))}>
          Async +100
        </button>
        <button onClick={() => dispatch(decrement())}>-</button>
      </div>
    </>
  );
};

export default Counter;
