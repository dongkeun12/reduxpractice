// 앱 하나에 하나의 store

import { configureStore } from "@reduxjs/toolkit";
import countReducer from "./features/counter/counterSlice";
import loginReducer from './features/login/loginSlice';
import {logger} from 'redux-logger';

export const store = configureStore({
  reducer: {
    counter: countReducer,
    login: loginReducer,
    
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch; // 비동기 dispatch작업에 필요한 타입
